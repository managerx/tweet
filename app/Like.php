<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
	use SoftDeletes;
    //
    protected $fillable  = [
    	
    	'message_id',
    	'user_id',
    	'like',
    	
    ];

     public function user(){
    	return $this->belongsto('App\User');
    }
    public function messages()
    {
        return $this->belongsto('App\Message');
    }
}
