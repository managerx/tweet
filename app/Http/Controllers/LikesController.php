<?php

namespace App\Http\Controllers;

use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function likeMessage($id)
    {
        // here you can check if product exists or is valid or whatever

        $this->handleLike('App\Message', $id);
        return redirect()->back();
    }

    public function handleLike( $id)
    {
        $existing_like = Like::withTrashed()->whereMessageId($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_like)) {
            Like::create([
                'user_id'    => Auth::id(),
                'message_id' => $id,
                'like'       => '1',
            ]);
        } else {
            if (is_null($existing_like->deleted_at)) {
                $existing_like->delete();
            } else {
                $existing_like->restore();
            }
        }
    }
}
