<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable  = [
    	'titre',
    	'description',
    	'user_id',
    	'like',
        'file',
    	
    ];

    public function user(){
    	return $this->belongsto('App\User');
    }

    public function likes()
    {
        return $this->hasMany('App\like');
    }

    
}
